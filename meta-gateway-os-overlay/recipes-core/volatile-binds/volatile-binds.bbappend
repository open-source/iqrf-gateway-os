VOLATILE_BINDS += "/data/etc/timezone /etc/timezone\n\
  /data/usr/share/zoneinfo /usr/share/zoneinfo\n\
  /data/home/admin /home/admin\n\
  /data/etc/docker /etc/docker\n\
  /data/etc/mosquitto /etc/mosquitto\n\
  /data/var/lib/mosquitto /var/lib/mosquitto\n\
  /data/node-red /home/root/.node-red\n\
"

do_install:append() {
  install -d ${D}/data/etc/docker
  install -d ${D}/data/etc/mosquitto
  install -d ${D}/data/var/lib/mosquitto
  install -d ${D}/data/node-red
}

FILES:${PN}:append = " /data/etc/docker /data/etc/mosquitto /data/var/lib/mosquitto /data/node-red"
