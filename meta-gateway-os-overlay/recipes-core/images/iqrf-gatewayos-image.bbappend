# necessary iqrf components for base image
IMAGE_INSTALL += " \
		volatile-binds \
"

# config files to be stored in persistent partition
PERSISTENT_CONF_FILES_SHADOW = " \
  ${sysconfdir}/shadow \
  ${sysconfdir}/passwd \
"

PERSISTENT_CONF_FILES = " \
    ${sysconfdir}/localtime \
	${sysconfdir}/nginx/sites-available/default_server \
	${sysconfdir}/nginx/sites-enabled \
"

PERSISTENT_CONF_FILES_SYSTEMD = " \
	${sysconfdir}/systemd/system \
"

# look through PERSISTENT_CONF_FILES, create related symbolic links to /data,
# create a empty file in /data/ if the original config file does not exist.
set_user_group:append() {
    for conf in ${PERSISTENT_CONF_FILES_SHADOW}; do
        mkdir -p ${IMAGE_ROOTFS}/$(dirname $conf)
        mkdir -p ${IMAGE_ROOTFS}/data$(dirname $conf)
        if [ -e ${IMAGE_ROOTFS}$conf ]; then
            mv ${IMAGE_ROOTFS}$conf ${IMAGE_ROOTFS}/data$conf
        else
            # conf is a directory
            if echo $conf | grep -o -q '^.*/$'; then
                mkdir -p ${IMAGE_ROOTFS}/data$conf
            else
                touch ${IMAGE_ROOTFS}/data$conf
            fi
        fi
        ln -sf /data$conf ${IMAGE_ROOTFS}$(echo $conf | sed -e 's/\/$//')
    done
}

create_persistent_symbolic_links() {
    for conf in ${PERSISTENT_CONF_FILES}; do
        mkdir -p ${IMAGE_ROOTFS}/$(dirname $conf)
        mkdir -p ${IMAGE_ROOTFS}/data$(dirname $conf)
        if [ -e ${IMAGE_ROOTFS}$conf ]; then
            mv ${IMAGE_ROOTFS}$conf ${IMAGE_ROOTFS}/data$conf
        else
	    # conf is a directory
	    if echo $conf | grep -o -q '^.*/$'; then
	        mkdir -p ${IMAGE_ROOTFS}/data$conf
	    else
		touch ${IMAGE_ROOTFS}/data$conf
	    fi
	fi
	ln -sf /data$conf ${IMAGE_ROOTFS}$(echo $conf | sed -e 's/\/$//')
    done

    # Handle persistent logging, store log information to /data/storage/log
    # rather than /var/log.
    if [ ${@ oe.types.boolean('${VOLATILE_LOG_DIR}') } = False ]; then
        mkdir -p ${IMAGE_ROOTFS}/data/storage
        mv ${IMAGE_ROOTFS}/var/log ${IMAGE_ROOTFS}/data/storage
        ln -sf /data/storage/log ${IMAGE_ROOTFS}/var/log
    fi
}

systemd_preset_all:append() {
    for conf in ${PERSISTENT_CONF_FILES_SYSTEMD}; do
        mkdir -p ${IMAGE_ROOTFS}/$(dirname $conf)
        mkdir -p ${IMAGE_ROOTFS}/data$(dirname $conf)
        if [ -e ${IMAGE_ROOTFS}$conf ]; then
            mv ${IMAGE_ROOTFS}$conf ${IMAGE_ROOTFS}/data$conf
        else
            # conf is a directory
            if echo $conf | grep -o -q '^.*/$'; then
                mkdir -p ${IMAGE_ROOTFS}/data$conf
            else
                touch ${IMAGE_ROOTFS}/data$conf
            fi
        fi
        ln -sf /data$conf ${IMAGE_ROOTFS}$(echo $conf | sed -e 's/\/$//')
    done
}

# Create persistent symbolic links
ROOTFS_POSTPROCESS_COMMAND += "create_persistent_symbolic_links ;"
