#!/bin/sh

#docker run -it -e USER_ID=$(id -u) --rm -v"`pwd`:/home/builder" -w "/home/builder" -v"/opt/yocto:/opt" -v"/home/spinarr/.ssh:/etc/skel/.ssh:ro" kasproject/kas:latest /bin/sh -c "kas build iqrf-gateway-os-iqaros-config.yml"

# $1 yml
# $2 nice bitbake -c do_compile iqrf-gateway-controller
# $2 nice bitbake -c listtasks iqrf-gateway-controller
# $2 nice bitbake -D -v iqrf-gateway-controller
# $2 nice bitbake-layers show-layers
docker run -it -e USER_ID=$(id -u) --rm -v"`pwd`:/home/builder" -w "/home/builder" -v"/opt/yocto:/opt" -v"/home/spinarr/.ssh:/etc/skel/.ssh:ro" kasproject/kas:latest /bin/sh -c "kas shell $1 -c '$2'"
