#!/bin/sh

if [ ! -z "$1" ]; then
	kas shell iqrf-gateway-os-sunxi.yml -c "$@" 
else
	kas build iqrf-gateway-os-sunxi.yml
fi
