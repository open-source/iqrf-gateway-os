# necessary features for the dev image
#IMAGE_FEATURES += " \
#                  dev-pkgs \
#                  dbg-pkgs \
#                  tools-sdk \
#                  tools-debug \
#                  tools-profile \
#                  ptest-pkgs \
#                  debug-tweaks \
#                  bash-completion-pkgs \
#"

IMAGE_FEATURES += " \
                  dev-pkgs \
                  dbg-pkgs \
                  tools-sdk \
                  tools-debug \
                  debug-tweaks \
"

# necessary components for the package management
IMAGE_INSTALL += " \
                apt \
                gnupg \
                gdbserver \
"

# necessary components for the app image
IMAGE_INSTALL += " \
                nodejs-npm \
                node-red \
                python3-pip \
		cmake \
		git \
		mc \
"

# compiling all applications without optimization:
DEBUG_BUILD = "1"

# compiling a specific application without optimization:
DEBUG_BUILD:pn-busybox = "1"

# ensure that the binaries in the sysroot/packages don't have their debugging symbols stripped
#INHIBIT_SYSROOT_STRIP = "1"
#INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
#INHIBIT_PACKAGE_STRIP = "1"

#One can generate a stripped-down version of the filesystem for debugging
#using the following variables:
#IMAGE_GEN_DEBUGFS = "1"
#IMAGE_FSTYPES_DEBUGFS = "tar.bz2"
