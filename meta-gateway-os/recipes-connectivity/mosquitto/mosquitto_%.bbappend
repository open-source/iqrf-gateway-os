FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = "file://mosquitto.conf \
				  file://auth.conf \
				  file://passwd \
"

# added custom config
do_install:append() {
	install -d ${D}${sysconfdir}/mosquitto/conf.d
	install -m 0644 ${WORKDIR}/mosquitto.conf ${D}${sysconfdir}/mosquitto/mosquitto.conf
	install -m 0644 ${WORKDIR}/auth.conf ${D}${sysconfdir}/mosquitto/conf.d/auth.conf
	install -m 0644 ${WORKDIR}/passwd ${D}${sysconfdir}/mosquitto/passwd
}
