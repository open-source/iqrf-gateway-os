require iqrf-gatewayos-image-base.inc

# necessary iqrf components for base image
IMAGE_INSTALL += " \
	iqrf-gateway-controller \
	iqrf-gateway-daemon \
	iqrf-gateway-setter \
	iqrf-gateway-webapp \
	iqrf-gateway-uploader \
	iqrf-journal-reader \
	iqrf-cloud-provisioning \
	iqrf-scripts \
	nodejs \
	node-red \
	node-red-dashboard \
	docker-ce \
	python3-pip \
	python3-distutils \
	python3-docker-compose \
"
