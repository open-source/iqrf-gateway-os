IMAGE_FEATURES = " ssh-server-openssh"

IMAGE_INSTALL = "packagegroup-core-boot \
	packagegroup-base \
	${CORE_IMAGE_EXTRA_INSTALL} \
	kernel-modules \
	wpa-supplicant \
	shape shapeware \
	networkmanager \
	monit \
	nano \
	sudo \
	mosquitto \
	mosquitto-clients \
	avahi-daemon \
	rsyslog \
	networkmanager \
	networkmanager-ppp \
	networkmanager-wwan \
	modemmanager \
	wireguard-tools \
	wireguard-client-config \
	usb-modeswitch \
	coreutils \
	logrotate \
	watchdog \
	watchdog-config \
	jq \
	htop \
	bash \
	bash-completion \
	haveged \
	rng-tools \
	iotop \
	fake-hwclock \
	tzdata \
	tzdata-europe \
	wget rsync curl \
	screen \
	systemd-conf \
	mender-connect \
	base-config \
	googletest \
	i2c-tools \
	apt \
	gnupg \
	util-linux \
	less \
	findutils \
	apcupsd \
	openssh-sshd \
	linux-firmware-rtl8188 \
	tuptime \
"

GLIBC_GENERATE_LOCALES = "en_GB.UTF-8 en_US.UTF-8"

IMAGE_LINGUAS ?= "en-gb"

inherit core-image extrausers

PASSWD="admin"

# set admin password for all images
EXTRA_USERS_PARAMS += " \
	useradd -p '' admin; \
	groupadd admin; \
	usermod -p '$(openssl passwd -6 ${PASSWD})' admin; \
	usermod -a -G root,sudo,docker admin; \
"
