FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = "file://daemon.json \
"

do_install:append() {
    install -d ${D}/etc/docker
    install -m 0644 ${WORKDIR}/daemon.json ${D}/etc/docker
}
