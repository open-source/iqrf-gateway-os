SUMMARY = "A set of dashboard nodes for Node-RED"
# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "Apache-2.0 & ISC & MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=088ce976a5d78ef059391f1ca042eb3d \
                    file://node_modules/@socket.io/component-emitter/LICENSE;md5=c22e1612ebc02ae74b51d7abefe6d1f2 \
                    file://node_modules/@types/cookie/LICENSE;md5=d4a904ca135bb7bc912156fee12726f0 \
                    file://node_modules/@types/cors/LICENSE;md5=d4a904ca135bb7bc912156fee12726f0 \
                    file://node_modules/@types/node/LICENSE;md5=d4a904ca135bb7bc912156fee12726f0 \
                    file://node_modules/accepts/LICENSE;md5=bf1f9ad1e2e1d507aef4883fff7103de \
                    file://node_modules/base64id/LICENSE;md5=abb57c73ecaa9ddaa151a4e424935b47 \
                    file://node_modules/bytes/LICENSE;md5=013e95467eddb048f19a6f5b42820f86 \
                    file://node_modules/compressible/LICENSE;md5=ba0b78039307836d62c2c53de4218eb2 \
                    file://node_modules/compression/LICENSE;md5=0afd201e48c7d095454eed4ac1184e40 \
                    file://node_modules/cookie/LICENSE;md5=bc85b43b6f963e8ab3f88e63628448ca \
                    file://node_modules/cors/LICENSE;md5=947eb5e695dade432a500b12c510de85 \
                    file://node_modules/debug/LICENSE;md5=ddd815a475e7338b0be7a14d8ee35a99 \
                    file://node_modules/depd/LICENSE;md5=ebc30494fd072dc98368da73e1821715 \
                    file://node_modules/destroy/LICENSE;md5=d5eb22cf6cc99e645b98b28ee3503ddf \
                    file://node_modules/ee-first/LICENSE;md5=c8d3a30332ecb31cfaf4c0a06da18f5c \
                    file://node_modules/encodeurl/LICENSE;md5=272621efa0ff4f18a73221e49ab60654 \
                    file://node_modules/engine.io-parser/LICENSE;md5=436a2c205caafa17c010702768e0bed0 \
                    file://node_modules/engine.io/LICENSE;md5=c9f272e8d85e84d214436bc09de14478 \
                    file://node_modules/engine.io/node_modules/debug/LICENSE;md5=d85a365580888e9ee0a01fb53e8e9bf0 \
                    file://node_modules/engine.io/node_modules/ms/license.md;md5=fd56fd5f1860961dfa92d313167c37a6 \
                    file://node_modules/escape-html/LICENSE;md5=f8746101546eeb9e4f6de64bb8bdf595 \
                    file://node_modules/etag/LICENSE;md5=6e8686b7b13dd7ac8733645a81842c4a \
                    file://node_modules/fresh/LICENSE;md5=373c2cf0978b37e434394a43b4cbbdb4 \
                    file://node_modules/gridstack/LICENSE;md5=83a7a20bae5e8fc4dc5518f9f5dffd92 \
                    file://node_modules/http-errors/LICENSE;md5=607209623abfcc77b9098f71a0ef52f9 \
                    file://node_modules/inherits/LICENSE;md5=5b2ef2247af6d355ae9d9f988092d470 \
                    file://node_modules/jquery/LICENSE.txt;md5=de877aa6d744cc160ff41c26a8e4811f \
                    file://node_modules/mime-db/LICENSE;md5=175b28b58359f8b4a969c9ab7c828445 \
                    file://node_modules/mime-types/LICENSE;md5=bf1f9ad1e2e1d507aef4883fff7103de \
                    file://node_modules/mime/LICENSE;md5=8e8ea2ad138ce468f8570a0edbadea65 \
                    file://node_modules/ms/license.md;md5=fd56fd5f1860961dfa92d313167c37a6 \
                    file://node_modules/negotiator/LICENSE;md5=6417a862a5e35c17c904d9dda2cbd499 \
                    file://node_modules/object-assign/license;md5=a12ebca0510a773644101a99a867d210 \
                    file://node_modules/on-finished/LICENSE;md5=1b1f7f9cec194121fdf616b971df7a7b \
                    file://node_modules/on-headers/LICENSE;md5=c6e0ce1e688c5ff16db06b7259e9cd20 \
                    file://node_modules/parseurl/LICENSE;md5=e7842ed4f188e53e53c3e8d9c4807e89 \
                    file://node_modules/range-parser/LICENSE;md5=d4246fb961a4f121eef5ffca47f0b010 \
                    file://node_modules/safe-buffer/LICENSE;md5=badd5e91c737e7ffdf10b40c1f907761 \
                    file://node_modules/send/LICENSE;md5=5f1a8369a899b128aaa8a59d60d00b40 \
                    file://node_modules/send/node_modules/ms/license.md;md5=2b8bc52ae6b7ba58e1629deabd53986f \
                    file://node_modules/serve-static/LICENSE;md5=27b1707520b14d0bc890f4e75cd387b0 \
                    file://node_modules/setprototypeof/LICENSE;md5=4846f1626304c2c0f806a539bbc7d54a \
                    file://node_modules/socket.io-adapter/LICENSE;md5=afbe5b2f47d5cf306759fe2d435b5ee0 \
                    file://node_modules/socket.io-parser/LICENSE;md5=afbe5b2f47d5cf306759fe2d435b5ee0 \
                    file://node_modules/socket.io-parser/node_modules/debug/LICENSE;md5=d85a365580888e9ee0a01fb53e8e9bf0 \
                    file://node_modules/socket.io-parser/node_modules/ms/license.md;md5=fd56fd5f1860961dfa92d313167c37a6 \
                    file://node_modules/socket.io/LICENSE;md5=852d069fbb52f1d8ed1ac32fa4b9147c \
                    file://node_modules/socket.io/node_modules/debug/LICENSE;md5=d85a365580888e9ee0a01fb53e8e9bf0 \
                    file://node_modules/socket.io/node_modules/ms/license.md;md5=fd56fd5f1860961dfa92d313167c37a6 \
                    file://node_modules/statuses/LICENSE;md5=36e2bc837ce69a98cc33a9e140d457e5 \
                    file://node_modules/toidentifier/LICENSE;md5=1a261071a044d02eb6f2bb47f51a3502 \
                    file://node_modules/vary/LICENSE;md5=13babc4f212ce635d68da544339c962b \
                    file://node_modules/ws/LICENSE;md5=0678f4fff95e1b65145631a8e2608d44"

SRC_URI = " \
    git://github.com/node-red/node-red-dashboard.git;protocol=https;branch=master \
    npmsw://${THISDIR}/${BPN}/npm-shrinkwrap.json \
    "

# Modify these as desired
PV = "3.6.0+git${SRCPV}"
SRCREV = "49350a821bf338d02702e1e7dc027348364701f6"

S = "${WORKDIR}/git"

inherit npm

LICENSE:${PN} = "Apache-2.0"
LICENSE:${PN}-socketio-component-emitter = "MIT"
LICENSE:${PN}-types-cookie = "MIT"
LICENSE:${PN}-types-cors = "MIT"
LICENSE:${PN}-types-node = "MIT"
LICENSE:${PN}-accepts = "MIT"
LICENSE:${PN}-base64id = "MIT"
LICENSE:${PN}-bytes = "MIT"
LICENSE:${PN}-compressible = "MIT"
LICENSE:${PN}-compression = "MIT"
LICENSE:${PN}-cookie = "MIT"
LICENSE:${PN}-cors = "MIT"
LICENSE:${PN}-debug = "MIT"
LICENSE:${PN}-depd = "MIT"
LICENSE:${PN}-destroy = "MIT"
LICENSE:${PN}-ee-first = "MIT"
LICENSE:${PN}-encodeurl = "MIT"
LICENSE:${PN}-engineio-debug = "MIT"
LICENSE:${PN}-engineio-ms = "MIT"
LICENSE:${PN}-engineio = "MIT"
LICENSE:${PN}-engineio-parser = "MIT"
LICENSE:${PN}-escape-html = "MIT"
LICENSE:${PN}-etag = "MIT"
LICENSE:${PN}-fresh = "MIT"
LICENSE:${PN}-gridstack = "MIT"
LICENSE:${PN}-http-errors = "MIT"
LICENSE:${PN}-inherits = "ISC"
LICENSE:${PN}-jquery = "MIT"
LICENSE:${PN}-mime = "MIT"
LICENSE:${PN}-mime-db = "MIT"
LICENSE:${PN}-mime-types = "MIT"
LICENSE:${PN}-ms = "MIT"
LICENSE:${PN}-negotiator = "MIT"
LICENSE:${PN}-object-assign = "MIT"
LICENSE:${PN}-on-finished = "MIT"
LICENSE:${PN}-on-headers = "MIT"
LICENSE:${PN}-parseurl = "MIT"
LICENSE:${PN}-range-parser = "MIT"
LICENSE:${PN}-safe-buffer = "MIT"
LICENSE:${PN}-send-ms = "MIT"
LICENSE:${PN}-send = "MIT"
LICENSE:${PN}-serve-static = "MIT"
LICENSE:${PN}-setprototypeof = "ISC"
LICENSE:${PN}-socketio-debug = "MIT"
LICENSE:${PN}-socketio-ms = "MIT"
LICENSE:${PN}-socketio = "MIT"
LICENSE:${PN}-socketio-adapter = "MIT"
LICENSE:${PN}-socketio-parser-debug = "MIT"
LICENSE:${PN}-socketio-parser-ms = "MIT"
LICENSE:${PN}-socketio-parser = "MIT"
LICENSE:${PN}-statuses = "MIT"
LICENSE:${PN}-toidentifier = "MIT"
LICENSE:${PN}-vary = "MIT"
LICENSE:${PN}-ws = "MIT"
