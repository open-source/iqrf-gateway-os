#!/bin/sh

docker run -it -e USER_ID=$(id -u) --rm -v "/home/marek/.ssh:/etc/skel/.ssh:ro" -v"`pwd`:/home/builder" -v"/home/marek/projects/downloads:/home/builder/build/downloads" -w "/home/builder" -v "/opt:/opt" kas_devel:0.1 sh -c "kas shell iqrf-gateway-os-config.yml -c '$1'"
