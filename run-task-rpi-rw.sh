#do_build                              Default task for a recipe - depends on all other normal tasks required to 'build' a recipe
#do_checkuri                           Validates the SRC_URI value
#do_clean                              Removes all output files for a target
#do_cleanall                           Removes all output files, shared state cache, and downloaded source files for a target
#do_cleansstate                        Removes all output files and shared state cache for a target
#do_compile                            Compiles the source in the compilation directory
#do_configure                          Configures the source by enabling and disabling any build-time and configuration options for the software being built
#do_deploy_source_date_epoch
#do_deploy_source_date_epoch_setscene   (setscene version)
#do_devpyshell                         Starts an interactive Python shell for development/debugging
#do_devshell                           Starts a shell with the environment set up for development/debugging
#do_fetch                              Fetches the source code
#do_generate_toolchain_file
#do_install                            Copies files from the compilation directory to a holding area
#do_listtasks                          Lists all defined tasks for a target
#do_package                            Analyzes the content of the holding area and splits it into subsets based on available packages and files
#do_package_qa                         Runs QA checks on packaged files
#do_package_qa_setscene                Runs QA checks on packaged files (setscene version)
#do_package_setscene                   Analyzes the content of the holding area and splits it into subsets based on available packages and files (setscene version)
#do_package_write_ipk                  Creates the actual IPK packages and places them in the Package Feed area
#do_package_write_ipk_setscene         Creates the actual IPK packages and places them in the Package Feed area (setscene version)
#do_packagedata                        Creates package metadata used by the build system to generate the final packages
#do_packagedata_setscene               Creates package metadata used by the build system to generate the final packages (setscene version)
#do_patch                              Locates patch files and applies them to the source code
#do_populate_lic                       Writes license information for the recipe that is collected later when the image is constructed
#do_populate_lic_setscene              Writes license information for the recipe that is collected later when the image is constructed (setscene version)
#do_populate_sysroot                   Copies a subset of files installed by do_install into the sysroot in order to make them available to other recipes
#do_populate_sysroot_setscene          Copies a subset of files installed by do_install into the sysroot in order to make them available to other recipes (setscene version)
#do_prepare_recipe_sysroot
#do_unpack                             Unpacks the source code into a working directory

#$1 task (build, clean, compile, configure, fetch, install ...)
#$2 target (iqrf-gateway-daemon, controller, translator ...)
#-D (debug)

./build_yocto_docker-debug.sh "iqrf-gateway-os-rpi-rw.yml" "nice bitbake -v -c $1 $2"
