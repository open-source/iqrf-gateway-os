# iqrf-gateway-os

Dedicated Yocto image for the IQRF gateways.

Support for IQD-GW-0x and RPI gateway types by the default.

## Acknowledgement

This project has been made possible with a government grant by means of [the Ministry of Industry and Trade of the Czech Republic](https://www.mpo.cz/) in [the TRIO program](https://starfos.tacr.cz/cs/project/FV40132).
