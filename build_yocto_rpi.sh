#!/bin/sh

if [ ! -z "$1" ]; then
	kas shell iqrf-gateway-os-rpi.yml -c "$@" 
else
	kas build iqrf-gateway-os-rpi.yml
fi
